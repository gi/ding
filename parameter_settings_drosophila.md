# Parameter Settings for Drosophila Experiment


## LASTZ Parameters

`--step=10 --gapped --gfextend --ambiguous=iupac --masking=5 --filter=identity:65 --hspthresh=10000`

## LASTZ Soring Scheme

see [lastz.scores](lastz.scores)

## GEESE Parameters

` --minLength 500 --minIdent 65 --maxGap 50 --minAlnLength 20`

