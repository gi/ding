# DCJ-indel for natural genomes

Compute the DCJ-indel distance of genomes containing an arbitrary number of markers.

_A python3 version of ding is available at [dingiiofficial](https://gitlab.ub.uni-bielefeld.de/gi/dingiiofficial) ._

An overview of the scripts dealing directly with the DING ILP:


|script  | purpose | input | output | dependencies |
| ------ | ------ | ------ | ------ | ------ |
|  unimog\_to\_ilp.py | Generate the DING ILP | [UniMoG file](https://bibiserv.cebitec.uni-bielefeld.de/dcj?id=dcj_manual) containing a single genome pair | [CPLEX lp file](https://www.ibm.com/support/knowledgecenter/SSSA5P_12.9.0/ilog.odms.cplex.help/CPLEX/FileFormats/topics/LP_MIP.html), UniMoG file with uniquely labeled genes | python2, ilp_util |
| parse\_cplex\_sol.py | Calculate the distance and matching | UniMoG file with uniquely labeled genes, [CPLEX solution file](https://www.ibm.com/support/knowledgecenter/SSSA5P_12.7.1/ilog.odms.cplex.help/CPLEX/FileFormats/topics/SOL.html) containing a single solution | UniMoG file with relabeled genes according to matching, (distance file) | python2, ilp_util|
| ilp_util.py | Provide utility functions for generation and interpretation| - | - | python2 |

A Typical workflow would look like this:
1.  Generate the ILP: `./unimog_to_ilp.py -i {unimog_example} -u {out_id_file} -o {out_cplex_lp} -l {log}`
2.  Apply CPLEX: `cplex -c "read {out_cplex_lp} lp" "mipopt" "write {solutionfile}.sol"`
3.  Get the matching and distance: `./parse_cplex_sol.py -i {solutionfile}.sol -u {out_id_file} -o {out_unimog_relabeled}`

The work of this repository is described in the following paper:
* Bohnenkämper L., Braga M.D.V., Doerr D., Stoye J.: Computing the Rearrangement Distance of Natural Genomes. In: Schwartz R. (eds) Proc. of RECOMB 2020. LNCS 12074, 3-18. Springer Verlag, 2020. [DOI](https://doi.org/10.1007/978-3-030-45257-5_1)

For a more detailed description see the following arxiv preprint:
*  Bohnenkämper, L., Braga, M.D.V., Doerr, D., Stoye, J.: Computing the rearrangement distance of natural genomes. [arXiv:2001.02139](http://arxiv.org/abs/2001.02139) (2020)
